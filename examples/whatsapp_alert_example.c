#include <stdio.h>
#include "whatsapp_alert.h"

int main()
{
    char *messages[5] = {
        "is critical",
        "is safe now",
        "is breathing better now",
        "has been tested positive",
        "has been tested negative"
    };

    // WhatsApp Number format: 91xxxxxxxxxx
    char *number = "<a-number-starting-with-91>";

    char message[100];

    for(int i = 0; i < 1; i++) {
        message[0] = '\0';
        sprintf(message, "The Patient #%d %s", (i + 1), messages[i]);

        if(whatsappSendMessage(number, message)) {
            printf("Message sent!");
        } else {
            printf("Message wasn't sent.");
        }

    }
    

    printf("\n");

    return 0;
}