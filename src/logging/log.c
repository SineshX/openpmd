#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#include "log.h"

#define MAX_TIME_SIZE 256
#define MAX_MSG_SIZE 1024
#define LOG_FILE "/var/log/pmd/pmd.log"

/**
 * @brief      Writes to log.
 *
 * @param      msg   The message
 */
void writeToLog (char *msg, ...) 
{
        va_list ap;
        char message[MAX_MSG_SIZE];

        va_start(ap, msg);
        vsnprintf(message, sizeof(message), msg, ap);
        va_end(ap);

        /* Declare a file pointer variable */
        FILE *fptr;

        /* Open the error database file */
        if ((fptr=fopen(LOG_FILE, "a+")) == NULL) {
                /* Print error if unable to open the file*/
                fprintf(stderr, "Failed to open file %s", LOG_FILE);

        }
        char buf[MAX_TIME_SIZE];

        /* Declare a variables for storing time info */
        time_t rawtime;
        struct tm * timeinfo;

        /* Get time from the system*/
        time(&rawtime);
        /* Convert this time into local time */
        timeinfo = localtime(&rawtime);

        /* Print time and message passed */
        strftime(buf,150,"%a %d %b %Y %X",timeinfo);
        fprintf(fptr,"%s: %s \n",buf,message);

        /* Close the file */
        fclose(fptr);
}
