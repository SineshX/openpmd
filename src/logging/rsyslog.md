## Configuration of rsyslog client 

* Install rsyslog 

    `sudo apt install rsyslog`
    
* Run the rsyslog 

    `sudo service rsyslog start`
    
* Check the status if its active. If not run rsyslog again

    `sudo service rsyslog status`

* Open the file /etc/resyslog.conf by the given command
    
    `sudo nano /etc/rsyslog.conf`

* Add the line where IP 18.223.3.241:514 is your server IP address

    `*.* @@18.223.3.241:514`

* Restart the rsyslog service
    
    `sudo service rsyslog restart`

* Add file you want to share on the rsyslog server. An example is given

    `logger -s " This is my Rsyslog client "`

## Configuration of Rsyslog Server
    
* Run the rsyslog service on the server side

    `sudo service rsyslog start`
    
* Check the status if its active. If not run rsyslog service again

    `sudo service rsyslog status`

* Open the file /etc/rsyslog.conf by the given command
    
    `sudo nano /etc/rsyslog.conf`
    
* Uncomment the following lines in the /etc/rsyslog.conf to allow log recepyion though UDP & TCP

```
    module(load="imudp")
    input(type="imudp" port="514")
    module(load="imtcp")
    input(type="imtcp" port="514")
    
```
* To sort the logs directory wise add the following lines in the /etc/rsyslog.conf

```
    $template RemoteLogs,"/var/log/%HOSTNAME%/.log"
    *.* ?RemoteLogs
    & ~

```
 
* Edit firewall rules to allow to allow logs reception through TCP run

    `sudo ufw allow 514/tcp`

* Restart the rsyslog server
    
    `sudo service rsyslog restart`
    
* Now you can view the logs shared by client by using 
    
    `sudo nano /var/log`

