#ifndef LOG_H
#define LOG_H

#ifdef __cplusplus
extern "C" {
#endif

extern void writeToLog (char *msg, ...);

#ifdef __cplusplus
}
#endif

#endif
