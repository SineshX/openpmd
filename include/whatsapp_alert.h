#include <stdio.h>
#include <curl/curl.h>
#include <string.h>

#define URL "https://api.gupshup.io/sm/api/v1/msg"
#define API_KEY "apikey: <YOUR-API-KEY-HERE>"
#define PARAMS "channel=whatsapp&source=917834811114&src.name=OpenVentiBeta"

CURL *curl;
CURLcode res;

int whatsappSendMessage(char *number, char *message);